//
//  FindOneViewController.swift
//  CoreDataProyecto
//
//  Created by Robert on 30/1/18.
//  Copyright © 2018 Robert. All rights reserved.
//

import UIKit

class FindOneViewController: UIViewController {

    @IBOutlet weak var addresLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    
    var person:Person?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = person?.name
        addresLabel.text = person?.addres
        phoneLabel.text = person?.phone
        // Do any additional setup after loading the view.
    }

    @IBAction func deleteButton(_ sender: Any) {
        let manageObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        manageObjectContext.delete(person!)
        do{
            try manageObjectContext.save()
            addresLabel.text = ""
            phoneLabel.text = ""
        }catch{
            print("Error deleting object")
        }
    }
    
    

}
