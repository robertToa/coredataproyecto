//
//  PersonTableViewCell.swift
//  CoreDataProyecto
//
//  Created by Robert on 30/1/18.
//  Copyright © 2018 Robert. All rights reserved.
//

import UIKit

class PersonTableViewCell: UITableViewCell {

    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var addresLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func fillDat(person: Person){
        nameLabel.text = person.name
        phoneLabel.text = person.addres
        addresLabel.text = person.phone
    }
    
}
