//
//  ViewController.swift
//  CoreDataProyecto
//
//  Created by Robert on 24/1/18.
//  Copyright © 2018 Robert. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {

    @IBOutlet weak var NameTextField: UITextField!
    @IBOutlet weak var AddresTextField: UITextField!
    @IBOutlet weak var PhoneTextField: UITextField!
    
    var singlePerson:Person?
    var pluralPerson:[Person] = []
    
    let manageObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    func savePerson(){
        let entityDescription = NSEntityDescription.entity(forEntityName: "Person", in: manageObjectContext)
        let person = Person(entity: entityDescription!, insertInto: manageObjectContext)
        
        person.addres =  AddresTextField.text ?? ""
        person.name = NameTextField.text ?? ""
        person.phone = PhoneTextField.text ?? ""
        
        do{
            try manageObjectContext.save()
            clearFields()
            
        } catch{
            print("Error")
        }
    }

    func clearFields(){
        AddresTextField.text = ""
        NameTextField.text = ""
        PhoneTextField.text = ""
    }
    
    func findAll(){
        let entityDescription = NSEntityDescription.entity(forEntityName: "Person", in: manageObjectContext)
        let request:NSFetchRequest<Person> = Person.fetchRequest()
        
        do{
            let results = try(manageObjectContext.fetch(request as! NSFetchRequest<NSFetchRequestResult>))
            
            for result in results {
                let person = result as! Person
                /*print(result)
                print("Name: \(person.name ?? "")", terminator: "")
                print("Address: \(person.addres ?? "")", terminator: "")
                print("Phone: \(person.phone ?? "")", terminator: "")*/
                pluralPerson.append(person)
            }
            performSegue(withIdentifier: "findAllSegue", sender: self)
            
        }catch{
            print("Error finding people")
        }
    }
    
    func FindOne() {
        let entityDescription = NSEntityDescription.entity(forEntityName: "Person", in: manageObjectContext)
        let request:NSFetchRequest<Person> = Person.fetchRequest()
        request.entity = entityDescription
        
        let predicate = NSPredicate(format: "name = %@", NameTextField.text!)
        
        request.predicate = predicate
        
        do{
            let results = try(manageObjectContext.fetch(request as! NSFetchRequest<NSFetchRequestResult>))
            
            if(results.count > 0)
            {
                let match = results[0] as! Person
                
                /*AddresTextField.text = match.addres
                NameTextField.text = match.name
                PhoneTextField.text = match.phone*/
                singlePerson = match
                performSegue(withIdentifier: "findOneSegue", sender: self)
                
            }else{
                AddresTextField.text = "n/a"
                NameTextField.text = "n/a"
                PhoneTextField.text = "n/a"
            }
            
        }catch{
            print("Error finding people")
        }
        
        
    }
    
    
    @IBAction func SaveButtonPresed(_ sender: Any) {
        savePerson()
    }

    @IBAction func FindButtonPresed(_ sender: Any) {
        if(NameTextField.text == "")
        {
            findAll()
        }else{
            FindOne()
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "findOneSegue"){
            let destination = segue.destination as! FindOneViewController
            destination.person = singlePerson
        }
        else{
            if (segue.identifier == "findAllSegue"){
                let destination = segue.destination as! FindAllViewController
                destination.Persons = pluralPerson
            }
        }
    }
    
    
    
}


