//
//  FindAllViewController.swift
//  CoreDataProyecto
//
//  Created by Robert on 30/1/18.
//  Copyright © 2018 Robert. All rights reserved.
//

import UIKit

class FindAllViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    var Persons:[Person] = []
    var personIndex = 0
    override func viewDidLoad() {
        super  .viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    
    func  numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        personIndex = indexPath.row
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Persons.count
    }
    
    //metodo que devuelve data
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell  {
        let cell = tableView.dequeueReusableCell(withIdentifier: "celda", for: indexPath) as! PersonTableViewCell
        print("la persona es: \(Persons[indexPath.row])")
        cell.fillDat(person: Persons[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "section 1"
        default:
            return "section 2"
        }
    }
    

   
}
